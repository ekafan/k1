import java.util.Arrays;

public class ColorSort {

    enum Color {red, green, blue}

    public static void main(String[] param) {

        Color[] listColors = {Color.red, Color.red, Color.green, Color.red, Color.blue, Color.green};
        reorder(listColors);
        System.out.println(Arrays.toString(listColors));

        Color[] listColors2 = {Color.blue, Color.green, Color.red, Color.red, Color.green, Color.red};
        reorder(listColors2);
        System.out.println(Arrays.toString(listColors2));

    }

    public static void reorder(Color[] balls) {

        int red = 0;
        int green = 0;
        int blue = 0;

        for (Color color : balls) {
            if (color.equals(Color.red)) {
                red += 1;
            } else if (color.equals(Color.green)) {
                green += 1;
            } else {
                blue += 1;
            }
        }
        Color[] colorRed = new Color[red];
        Arrays.fill(colorRed, Color.red);
        Color[] colorGreen = new Color[green];
        Arrays.fill(colorGreen, Color.green);
        Color[] colorBlue = new Color[blue];
        Arrays.fill(colorBlue, Color.blue);

        System.arraycopy(colorRed, 0, balls, 0, colorRed.length);
        System.arraycopy(colorGreen, 0, balls, colorRed.length, colorGreen.length);
        System.arraycopy(colorBlue, 0, balls, colorRed.length + colorGreen.length, colorBlue.length);

    }
}
